Feature: Hear shout
    Shouty allows users to "hear" other users "shouts" as long as they are close enough to each other.

    Rule: Shouts can be heard by other users

        Scenario: Listener is within range
            Given a person named Sean
            And a person named Lucy
            When Sean shouts "free bagels at Sean's"
            Then Lucy hears Sean's message

        Scenario: Listener hears a different message
            Given a person named Sean
            And a person named Lucy
            When Sean shouts "free coffee!"
            Then Lucy hears Sean's message

    Rule: Shouts should only be heard if listener is within range

        Scenario: Listener is within range
            Given the range is 100 meters
            And people are located at
                | name     | Sean | Lucy |
                | location | 0    | 50   |
            When Sean shouts
            Then Lucy hears a shout

        Scenario: Listener is out of range
            Given the range is 100 meters
            And people are located at
                | name     | Sean | Gerry |
                | location | 0    | 150   |
            When Sean shouts
            Then Gerry should not hears a shout

    Rule: Listener should be able to hear multiple shouts

        Scenario: Two shouts
            Given a person named Sean
            And a person named Lucy
            When Sean shouts "free bagels!"
            And Sean shouts "free coffee!"
            Then Lucy hears the following messages:
                | free bagels! |
                | free coffee! |

    Rule: Maximum length of message is 180 characters
        Scenario: Message is too long
            Given a person named Sean
            And a person named Lucy
            When Sean shouts following message
                """
                Because all of me loves all of you
                Loves your curves and your edges
                All your perfect imperfections
                Give your all to me
                I'll give my all to you
                You're my end and my beginning
                Even when I lose I'm winning
                """
            Then Lucy should not hears a shout